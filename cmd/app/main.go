package main

import (
	"gitlab.com/todo-group/user-service/config"
	"gitlab.com/todo-group/user-service/internal/app"
)

func main() {
	// Configuration
	cfg := config.LoadConfig()

	// Run
	app.Run(cfg)
}
