FROM golang:1.19.3-alpine
RUN mkdir user
COPY . /user
WORKDIR /user
RUN go build -o main cmd/app/main.go
CMD ./main
EXPOSE 9998