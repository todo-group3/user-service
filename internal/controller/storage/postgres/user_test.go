package postgres

import (
	"fmt"
	"testing"

	"github.com/bxcodec/faker/v4"
	"github.com/google/uuid"
	"github.com/stretchr/testify/suite"
	"gitlab.com/todo-group/user-service/config"
	"gitlab.com/todo-group/user-service/genproto/user"
	"gitlab.com/todo-group/user-service/internal/controller/storage/repo"
	"gitlab.com/todo-group/user-service/pkg/db"
)

type UserRepositoryTestSuit struct {
	suite.Suite
	CleanUpFunc func()
	Repository  repo.UserStorageI
}

func (s *UserRepositoryTestSuit) SetUpSuit() {
	cfg := config.LoadConfig()
	pgxUrl := fmt.Sprintf("postgres://%s:%s@%s:%s/%s",
		cfg.PostgresUser,
		cfg.PostgresPassword,
		cfg.PostgresHost,
		cfg.PostgresPort,
		cfg.PostgresDatabase,
	)

	pg, err := db.New(pgxUrl, db.MaxPoolSize(cfg.PGXPoolMax))
	if err != nil {
		fmt.Println(err)
	}
	s.Repository = NewUserRepo(pg)
	s.CleanUpFunc = func() {
		pg.Close()
	}
}

func (s *UserRepositoryTestSuit) TestUserCRUD() {
	userReq := user.CreateUserReq{
		Id:           uuid.New().String(),
		FirstName:    faker.Name(),
		LastName:     faker.Name(),
		Username:     faker.Username(),
		Email:        faker.Email(),
		ProfilePhoto: faker.URL(),
		Password:     faker.Password(),
		RefreshToken: faker.Sentence(),
	}

	res, err := s.Repository.CreateUser(&userReq)
	s.Nil(err)
	s.NotEmpty(res)
	s.Equal(res.Id, userReq.Id)
	s.Equal(res.Email, userReq.Email)
	s.Equal(res.Username, userReq.Username)
	s.Equal(res.ProfilePhoto, userReq.ProfilePhoto)
	s.Equal(res.FirstName, userReq.FirstName)

	updateUserRequest := &user.UpdateUserReq{
		Id:           userReq.Id,
		FirstName:    "new first name",
		LastName:     "New last name",
		Username:     faker.Username(),
		ProfilePhoto: faker.URL(),
	}

	new_res, err := s.Repository.UpdateUser(updateUserRequest)
	s.Nil(err)
	s.NotEmpty(new_res)
	s.Equal(new_res.Id, updateUserRequest.Id)
	s.Equal(new_res.FirstName, updateUserRequest.FirstName)
	s.Equal(new_res.LastName, updateUserRequest.LastName)
	s.Equal(new_res.Username, updateUserRequest.Username)
	s.Equal(new_res.ProfilePhoto, updateUserRequest.ProfilePhoto)
	s.NotEqual(new_res.UpdatedAt, res.UpdatedAt)

	isExist, err := s.Repository.CheckField(&user.FieldReq{
		Id:    new_res.Id,
		Key:   "email",
		Value: new_res.Email,
	})

	s.Nil(err)
	s.Equal(isExist.Exists, true)
	isExist, err = s.Repository.CheckField(&user.FieldReq{
		Id:    new_res.Id,
		Key:   "email",
		Value: new_res.Email + "aksdjflaj",
	})

	s.Nil(err)
	s.Equal(isExist.Exists, false)

	isExist, err = s.Repository.CheckField(&user.FieldReq{
		Id:    new_res.Id,
		Key:   "asdf",
		Value: new_res.Email + "aksdjflaj",
	})
	s.NotNil(err)
	s.Empty(isExist)

	_, err = s.Repository.UpdateFieldById(&user.FieldReq{
		Id:    new_res.Id,
		Key:   "email",
		Value: faker.Email(),
	})
	s.Nil(err)

	get_res, err := s.Repository.GetUserById(&user.GetUserReq{
		Value: new_res.Id,
	})
	s.Nil(err)
	s.NotEmpty(get_res)
	s.NotEqual(get_res.Email, new_res.Email)

	_, err = s.Repository.DeleteUser(&user.GetUserReq{
		Value: new_res.Id,
	})
	s.Nil(err)

	email_res, err := s.Repository.GetUserByEmail(&user.GetUserReq{
		Value: get_res.Email,
	})
	s.NotNil(err)
	s.Empty(email_res)
}

func (s *UserRepositoryTestSuit) TearDown() {
	s.CleanUpFunc()
}

func TestUserRepositoryTestSuit(t *testing.T) {
	test := &UserRepositoryTestSuit{}
	test.SetUpSuit()
	suite.Run(t, test)
}
