package postgres

import (
	"gitlab.com/todo-group/user-service/pkg/db"
)

type UserRepo struct {
	Db *db.Postgres
}

func NewUserRepo(pDb *db.Postgres) *UserRepo {
	return &UserRepo{Db: pDb}
}
