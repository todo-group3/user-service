package postgres

import (
	"context"
	"database/sql"
	"fmt"
	"time"

	"gitlab.com/todo-group/user-service/genproto/user"
)

func (s *UserRepo) CreateUser(req *user.CreateUserReq) (*user.CreateUserRes, error) {
	var (
		created_at time.Time
		updated_at time.Time
		res        = &user.CreateUserRes{}
	)

	query, _, err := s.Db.Builder.Insert("users").Columns(
		"id",
		"first_name",
		"last_name",
		"username",
		"email",
		"profile_photo",
		"refresh_token",
		"password",
	).Values(
		req.Id,
		req.FirstName,
		req.LastName,
		req.Username,
		req.Email,
		req.ProfilePhoto,
		req.RefreshToken,
		req.Password,
	).Suffix(`returning 
	id,
	first_name,
	last_name,
	username,
	email,
	profile_photo,
	refresh_token,
	created_at,
	updated_at`,
	).ToSql()
	if err != nil {
		return &user.CreateUserRes{}, err
	}

	err = s.Db.Pool.QueryRow(context.Background(), query,
		req.Id,
		req.FirstName,
		req.LastName,
		req.Username,
		req.Email,
		req.ProfilePhoto,
		req.RefreshToken,
		req.Password,
	).Scan(
		&res.Id,
		&res.FirstName,
		&res.LastName,
		&res.Username,
		&res.Email,
		&res.ProfilePhoto,
		&res.RefreshToken,
		&created_at,
		&updated_at,
	)
	res.CreatedAt = fmt.Sprintf("%v", created_at)
	res.UpdatedAt = fmt.Sprintf("%v", updated_at)

	return res, err
}

func (s *UserRepo) GetUserByEmail(req *user.GetUserReq) (*user.GetUserByEmailRes, error) {
	var (
		created_at time.Time
		updated_at time.Time
		res        = &user.GetUserByEmailRes{}
	)

	query, _, err := s.Db.Builder.Select(
		"id",
		"first_name",
		"last_name",
		"username",
		"email",
		"profile_photo",
		"refresh_token",
		"password",
		"created_at",
		"updated_at",
	).From("users").Where("email=$1 and deleted_at is null", req.Value).ToSql()
	if err != nil {
		return &user.GetUserByEmailRes{}, err
	}

	err = s.Db.Pool.QueryRow(context.Background(), query, req.Value).Scan(
		&res.Id,
		&res.FirstName,
		&res.LastName,
		&res.Username,
		&res.Email,
		&res.ProfilePhoto,
		&res.RefreshToken,
		&res.Password,
		&created_at,
		&updated_at,
	)
	if err != nil {
		return &user.GetUserByEmailRes{}, err
	}

	res.CreatedAt = fmt.Sprintf("%v", created_at)
	res.UpdatedAt = fmt.Sprintf("%v", updated_at)

	return res, nil
}

func (s *UserRepo) GetUserById(req *user.GetUserReq) (*user.GetUserByIdRes, error) {
	var (
		created_at time.Time
		updated_at time.Time
		res        = &user.GetUserByIdRes{}
	)

	query, _, err := s.Db.Builder.Select(
		"id",
		"first_name",
		"last_name",
		"username",
		"email",
		"profile_photo",
		"created_at",
		"updated_at",
	).From("users").Where("id=$1 and deleted_at is null", req.Value).ToSql()
	if err != nil {
		return &user.GetUserByIdRes{}, err
	}

	err = s.Db.Pool.QueryRow(context.Background(), query, req.Value).Scan(
		&res.Id,
		&res.FirstName,
		&res.LastName,
		&res.Username,
		&res.Email,
		&res.ProfilePhoto,
		&created_at,
		&updated_at,
	)
	if err != nil {
		return &user.GetUserByIdRes{}, err
	}

	res.CreatedAt = fmt.Sprintf("%v", created_at)
	res.UpdatedAt = fmt.Sprintf("%v", updated_at)

	return res, nil
}

func (s *UserRepo) UpdateUser(req *user.UpdateUserReq) (*user.GetUserByIdRes, error) {
	var (
		created_at time.Time
		updated_at time.Time
		res        = &user.GetUserByIdRes{}
	)

	query, _, err := s.Db.Builder.Update("users").Set(
		"first_name", req.FirstName,
	).Set(
		"last_name", req.LastName,
	).Set(
		"username", req.Username,
	).Set(
		"profile_photo", req.ProfilePhoto,
	).Set(
		"updated_at", time.Now(),
	).Where("id=$6 and deleted_at is null", req.Id).Suffix(
		`returning 
	id,
	first_name,
	last_name,
	username,
	email,
	profile_photo,
	created_at,
	updated_at`,
	).ToSql()
	if err != nil {
		return &user.GetUserByIdRes{}, err
	}
	err = s.Db.Pool.QueryRow(context.Background(), query,
		req.FirstName,
		req.LastName,
		req.Username,
		req.ProfilePhoto,
		time.Now(),
		req.Id).Scan(
		&res.Id,
		&res.FirstName,
		&res.LastName,
		&res.Username,
		&res.Email,
		&res.ProfilePhoto,
		&created_at,
		&updated_at,
	)
	if err != nil {
		return &user.GetUserByIdRes{}, err
	}

	res.CreatedAt = fmt.Sprintf("%v", created_at)
	res.UpdatedAt = fmt.Sprintf("%v", updated_at)

	return res, err
}

func (s *UserRepo) UpdateFieldById(req *user.FieldReq) (*user.Empty, error) {
	query, _, err := s.Db.Builder.Update("users").Set(req.Key, req.Value).Where("id=$2", req.Id).ToSql()
	if err != nil {
		return &user.Empty{}, err
	}
	_, err = s.Db.Pool.Exec(context.Background(), query, req.Value, req.Id)
	if err != nil {
		return &user.Empty{}, err
	}

	return &user.Empty{}, nil
}

func (s *UserRepo) CheckField(req *user.FieldReq) (*user.CheckFieldRes, error) {
	query := fmt.Sprintf("SELECT 1 FROM users where %s = $1", req.Key)
	rows, err := s.Db.Pool.Query(context.Background(), query, req.Value)
	if err == sql.ErrNoRows {
		return &user.CheckFieldRes{Exists: false}, nil
	}
	if err != nil {
		return &user.CheckFieldRes{Exists: false}, err
	}
	defer rows.Close()

	for rows.Next() {
		temp := 0
		err = rows.Scan(&temp)
		if err != nil {
			return &user.CheckFieldRes{Exists: false}, err
		}
		if temp == 1 {
			return &user.CheckFieldRes{
				Exists: true,
			}, nil
		}
	}
	return &user.CheckFieldRes{Exists: false}, nil
}

func (s *UserRepo) DeleteUser(req *user.GetUserReq) (*user.Empty, error) {
	query := `UPDATE users SET deleted_at=NOW() where id = $1 and deleted_at is null`

	_, err := s.Db.Pool.Exec(context.Background(), query, req.Value)

	return &user.Empty{}, err
}

// Only field name and id is enough
func (s *UserRepo) GetFieldById(req *user.FieldReq) (*user.GetFieldByIdRes, error) {
	query := fmt.Sprintf("SELECT %s FROM users where id = $1 and deleted_at is null", req.Key)
	response := &user.GetFieldByIdRes{}
	err := s.Db.Pool.QueryRow(context.Background(), query, req.Id).Scan(
		&response.Value,
	)
	
	return response, err
}
