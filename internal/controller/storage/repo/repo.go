package repo

import "gitlab.com/todo-group/user-service/genproto/user"

type UserStorageI interface {
	CreateUser(req *user.CreateUserReq) (*user.CreateUserRes, error)
	GetUserByEmail(req *user.GetUserReq) (*user.GetUserByEmailRes, error)
	GetUserById(req *user.GetUserReq) (*user.GetUserByIdRes, error)
	UpdateUser(req *user.UpdateUserReq) (*user.GetUserByIdRes, error)
	UpdateFieldById(req *user.FieldReq) (*user.Empty, error)
	CheckField(req *user.FieldReq) (*user.CheckFieldRes, error)
	DeleteUser(req *user.GetUserReq) (*user.Empty, error)
	GetFieldById(req *user.FieldReq) (*user.GetFieldByIdRes, error)
}
