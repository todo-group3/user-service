package storage

import (
	"gitlab.com/todo-group/user-service/internal/controller/storage/postgres"
	"gitlab.com/todo-group/user-service/internal/controller/storage/repo"
	"gitlab.com/todo-group/user-service/pkg/db"
)

type IStorage interface {
	User() repo.UserStorageI
}

type StoragePg struct {
	Db       *db.Postgres
	UserRepo repo.UserStorageI
}

// NewStoragePg
func NewStoragePg(pDb *db.Postgres) *StoragePg {
	return &StoragePg{
		Db:       pDb,
		UserRepo: postgres.NewUserRepo(pDb),
	}
}

func (s StoragePg) User() repo.UserStorageI {
	return s.UserRepo
}
