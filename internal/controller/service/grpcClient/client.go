package grpcclient

import (
	"gitlab.com/todo-group/user-service/config"
)

type Clients interface {
}

type ServiceManager struct {
	Config config.Config
}

func New(c config.Config) (Clients, error) {
	return ServiceManager{}, nil
}
