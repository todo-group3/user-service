package service

import (
	grpcclient "gitlab.com/todo-group/user-service/internal/controller/service/grpcClient"
	"gitlab.com/todo-group/user-service/internal/controller/storage"
	"gitlab.com/todo-group/user-service/internal/rabbitmq"
	"gitlab.com/todo-group/user-service/pkg/db"
	"gitlab.com/todo-group/user-service/pkg/logger"
)

type UserService struct {
	Logger   *logger.Logger
	Clients  grpcclient.Clients
	Storage  storage.IStorage
	Rabbitmq *rabbitmq.RabbitMQ
}

func NewUserService(l *logger.Logger, client grpcclient.Clients, stg *db.Postgres, mq *rabbitmq.RabbitMQ) *UserService {
	return &UserService{
		Logger:   l,
		Clients:  client,
		Storage:  storage.NewStoragePg(stg),
		Rabbitmq: mq,
	}
}
