package service

import (
	"context"
	"fmt"

	"gitlab.com/todo-group/user-service/genproto/user"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"
)

func (s *UserService) CreateUser(ctx context.Context, req *user.CreateUserReq) (*user.CreateUserRes, error) {
	res, err := s.Storage.User().CreateUser(req)
	if err != nil {
		s.Logger.Error(fmt.Errorf("service : CreateUser : %w", err))
		return &user.CreateUserRes{}, status.Error(codes.Internal, "Couldn't create user")
	}
	return res, nil
}

func (s *UserService) GetUserByEmail(ctx context.Context, req *user.GetUserReq) (*user.GetUserByEmailRes, error) {
	res, err := s.Storage.User().GetUserByEmail(req)
	if err != nil {
		s.Logger.Error(fmt.Errorf("service : GetUserByEmail : %w", err))
		return &user.GetUserByEmailRes{}, status.Error(codes.Internal, "Couldn't get user")
	}
	return res, nil
}

func (s *UserService) GetUserById(ctx context.Context, req *user.GetUserReq) (*user.GetUserByIdRes, error) {
	res, err := s.Storage.User().GetUserById(req)
	if err != nil {
		s.Logger.Error(fmt.Errorf("service : GetUserById : %w", err))
		return &user.GetUserByIdRes{}, status.Error(codes.Internal, "Couldn't get user")
	}
	return res, nil
}

func (s *UserService) UpdateUser(ctx context.Context, req *user.UpdateUserReq) (*user.GetUserByIdRes, error) {
	res, err := s.Storage.User().UpdateUser(req)
	if err != nil {
		s.Logger.Error(fmt.Errorf("service : UpdateUser : %w", err))
		return &user.GetUserByIdRes{}, status.Error(codes.Internal, "Couldn't update user")
	}
	return res, nil
}

func (s *UserService) UpdateFieldById(ctx context.Context, req *user.FieldReq) (*user.Empty, error) {
	res, err := s.Storage.User().UpdateFieldById(req)
	if err != nil {
		s.Logger.Error(fmt.Errorf("service : UpdateFieldById : %w", err))
		return &user.Empty{}, status.Error(codes.Internal, "Couldn't update user field")
	}
	return res, nil
}

func (s *UserService) CheckField(ctx context.Context, req *user.FieldReq) (*user.CheckFieldRes, error) {
	res, err := s.Storage.User().CheckField(req)
	if err != nil {
		s.Logger.Error(fmt.Errorf("service : CheckField : %w", err))
		return &user.CheckFieldRes{Exists: false}, status.Error(codes.Internal, "Couldn't check user field")
	}
	return res, nil
}

func (s *UserService) DeleteUser(ctx context.Context, req *user.GetUserReq) (*user.Empty, error) {
	res, err := s.Storage.User().DeleteUser(req)
	if err != nil {
		s.Logger.Error(fmt.Errorf("service : DeleteUser : %w", err))
		return &user.Empty{}, status.Error(codes.Internal, "Couldn't delete user")
	}
	return res, nil
}

func (s *UserService) SendEmailVerification(ctx context.Context, req *user.VerifyReq) (*user.Empty, error) {
	res, err := s.Rabbitmq.SendEmailVerification(req)
	if err != nil {
		s.Logger.Error(fmt.Errorf("service.SendEmailVerification - %w", err))
		return &user.Empty{}, status.Error(codes.Internal, "Couldn't send email verification to user")
	}
	return res, nil
}

func (s *UserService) GetFieldById(ctx context.Context, req *user.FieldReq) (*user.GetFieldByIdRes, error) {
	res, err := s.Storage.User().GetFieldById(req)
	if err != nil {
		s.Logger.Error(fmt.Errorf("service.GetFieldById - %w", err))
		return &user.GetFieldByIdRes{}, status.Error(codes.Internal, "Couldn't get the field by id")
	}

	return res, nil
}
