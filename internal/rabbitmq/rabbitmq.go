package rabbitmq

import (
	"encoding/json"
	"fmt"

	"github.com/streadway/amqp"
	"gitlab.com/todo-group/user-service/config"
	"gitlab.com/todo-group/user-service/genproto/user"
	"gitlab.com/todo-group/user-service/pkg/logger"
	messagebroker "gitlab.com/todo-group/user-service/pkg/message_broker"
)

type RabbitMQ struct {
	Chan *amqp.Channel
	Log  *logger.Logger
	Cfg  *config.Config
}

func New(cfg *config.Config, log *logger.Logger) *RabbitMQ {
	conn, err := messagebroker.NewRabbitMQ(cfg)
	if err != nil {
		panic(err)
	}
	ch, err := conn.Conn.Channel()
	if err != nil {
		panic(err)
	}
	_, err = ch.QueueDeclare(
		cfg.VerifyTopic,
		false,
		false,
		false,
		false,
		nil,
	)
	if err != nil {
		panic(err) 
	}


	return &RabbitMQ{
		Chan: ch,
		Log:  log,
		Cfg:  cfg,
	}
}

func (r *RabbitMQ) SendEmailVerification(req *user.VerifyReq) (*user.Empty, error) {
	reqByte, err := json.Marshal(req)
	if err != nil {
		r.Log.Error(fmt.Errorf("rabbitmq : SendEmailVerification: %w", err))
		return &user.Empty{}, err
	}

	err = r.Chan.Publish(
		"",
		"verify",
		false,
		false,
		amqp.Publishing{
			ContentType: "application/json",
			Body:        reqByte,
		},
	)
	if err != nil {
		r.Log.Error(fmt.Errorf("rabbitmq: SendEmailVerification - publish := %w", err))
	}

	return &user.Empty{}, err
}
