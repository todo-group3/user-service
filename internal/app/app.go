// Package app configures and runs application.
package app

import (
	"fmt"
	"log"
	"net"

	"gitlab.com/todo-group/user-service/config"
	"gitlab.com/todo-group/user-service/genproto/user"
	"gitlab.com/todo-group/user-service/internal/controller/service"
	grpcclient "gitlab.com/todo-group/user-service/internal/controller/service/grpcClient"
	"gitlab.com/todo-group/user-service/internal/rabbitmq"
	"gitlab.com/todo-group/user-service/pkg/db"
	"gitlab.com/todo-group/user-service/pkg/logger"
	"google.golang.org/grpc"
	"google.golang.org/grpc/reflection"
)

// Run creates objects via constructors.
func Run(cfg *config.Config) {
	l := logger.New(cfg.LogLevel)

	// Repository
	// postgres://user:password@host:5432/database
	pgxUrl := fmt.Sprintf("postgres://%s:%s@%s:%s/%s",
		cfg.PostgresUser,
		cfg.PostgresPassword,
		cfg.PostgresHost,
		cfg.PostgresPort,
		cfg.PostgresDatabase)

	pg, err := db.New(pgxUrl, db.MaxPoolSize(cfg.PGXPoolMax))
	if err != nil {
		l.Fatal(fmt.Errorf("app - Run - postgres.New: %w", err))
	}
	defer pg.Close()

	clients, err := grpcclient.New(*cfg)
	if err != nil {
		l.Fatal(fmt.Errorf("app - Run - grpcclient.New: %w", err))
	}
	rmq := rabbitmq.New(cfg, l)

	userService := service.NewUserService(l, clients, pg, rmq)

	lis, err := net.Listen("tcp", ":"+cfg.UserServicePort)
	if err != nil {
		l.Fatal(fmt.Errorf("app - Run - grpcclient.New: %w", err))
	}

	c := grpc.NewServer()
	reflection.Register(c)
	user.RegisterUserServiceServer(c, userService)

	l.Info("Server is running on" + "port" + ": " + cfg.UserServicePort)

	if err := c.Serve(lis); err != nil {
		log.Fatal("Error while listening: ", err)
	}
}
